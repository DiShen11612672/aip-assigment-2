/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.assg1.database;

import uts.aip.assg1.controller.UserController;
import uts.aip.assg1.model.UserDTO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import uts.aip.assg1.Sha;

/**
 *
 * @author sunda
 * 
 * This class is the data source of user
 */
public class UserDAO {

    /**
     * find a user by its username
     * @param username
     * @return a userDTO if a user is found
     */
    public UserDTO find(String username) {
        String query = "select * " + "from accounts " + "where username = ?";
        UserDTO user = null;
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);
                ) {
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new UserDTO();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    /**
     * 
     * @return all users from accounts table
     */
    public ArrayList<UserDTO> findAll() {
        String query = "select * from accounts";
        ArrayList<UserDTO> users = new ArrayList<UserDTO>();
        UserDTO user = null;
        DataSource ds = null;
        try (Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(query)) {
            while (rs.next()) {
                user = new UserDTO();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                users.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

    /**
     * encrypt the password and insert it to database with username
     * @param username
     * @param password 
     */
    public void register(String username, String password) {
        String shaPassword = null;
        try {
            shaPassword = Sha.hash256(password);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
        }
        String query = "INSERT INTO accounts"
                + "(username, password) " + "VALUES"
                + "(?,?)";
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setString(1, username);
            ps.setString(2, shaPassword);
            try {
                ps.executeUpdate();
            } catch (Exception e) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * check the old password is correct
     * @param username
     * @param password
     * @return true if the old password is correct
     */
    public boolean matchPassword(String username, String password) {
        String query = "select password " + "from accounts " + "where username = ?";
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (password.equals(rs.getString("password"))) {
                    return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * reset the username's password
     * 
     * @param username
     * @param password 
     */
    public void changePassword(String username, String password) {
        String query = "UPDATE accounts " + "SET password = ? " + "where username = ?";
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, password);
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
