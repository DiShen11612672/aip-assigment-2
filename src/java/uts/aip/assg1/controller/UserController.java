package uts.aip.assg1.controller;

import uts.aip.assg1.model.UserDTO;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.*;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import uts.aip.assg1.database.UserDAO;

/**
 *
 * @author sunda
 *
 * This class is the domain logic of user
 */
@Named
@RequestScoped
public class UserController implements Serializable {

    private String username;
    private String password;

    public UserController() {
    }

    /**
     *
     * @return all users
     */
    public ArrayList<UserDTO> getAllUsers() {
        UserDAO userDAO = new UserDAO();
        return userDAO.findAll();
    }

    /**
     *
     * @return a user by its username
     */
    public UserDTO find() {
        UserDAO userDAO = new UserDAO();
        return userDAO.find(username);
    }

    /**
     * for user login
     * 
     * @return goes to index.xhtml
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(username, password);
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage("Incorrect username or password"));
            return null;
        }
        return "index";
    }
/**
 * for user logout
 * 
 * @return goes to index.xhtml or stay on the current page if error occurs
 */
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.logout();
            //context.getExternalContext().invalidateSession();
            return "index";
        } catch (ServletException e) {
            // (you could also log the exception to the server log)
            context.addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * check the username if is already exist, then register
     * @return login() to go back to index.xhtml if successfully registered, otherwise, stay on the current page
     */
    public String register() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        UserDAO userDAO = new UserDAO();
        if (userDAO.find(username) == null) {
            userDAO.register(username, password);
            return login();
        } else {
            context.addMessage(null, new FacesMessage("This username has been registered, please change another one."));
        }

        return null;
    }



//
    /**
     * refer from: http://www.mkyong.com/jsf2/multi-components-validator-in-jsf-2-0/
     * check if password and confirm password are the same
     * 
     * @param event 
     */
    public void validatePassword(ComponentSystemEvent event) {

        FacesContext fc = FacesContext.getCurrentInstance();

        UIComponent components = event.getComponent();

        // get password
        UIInput uiInputPassword = (UIInput) components.findComponent("password");
        String password = uiInputPassword.getLocalValue() == null ? ""
                : uiInputPassword.getLocalValue().toString();
        String passwordId = uiInputPassword.getClientId();

        // get confirm password
        UIInput uiInputConfirmPassword = (UIInput) components.findComponent("confirmPassword");
        String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? ""
                : uiInputConfirmPassword.getLocalValue().toString();

        // Let required="true" do its job.
        if (password.isEmpty() || confirmPassword.isEmpty()) {
            return;
        }

        if (!password.equals(confirmPassword)) {
            FacesMessage msg = new FacesMessage("Password must match confirm password");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            fc.addMessage(passwordId, msg);
            fc.renderResponse();
        }

    }

}
